import React,{ Component } from "react";
import { Redirect } from 'react-router'
import App from './components/App'


export default class RootRouter extends Component {
  render(){
    const isLogged = localStorage.getItem('access-token') ? true : false ;
    console.log(isLogged);
    return(
      isLogged ?  <App /> : <Redirect to='/login' />
    );
  }
}