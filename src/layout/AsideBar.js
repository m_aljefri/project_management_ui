import React, { Component } from 'react'
import { Link, NavLink } from 'react-router-dom';
import { connect } from 'react-redux';

class AsideBar extends Component{
  render(){
    return(
      <div className='aside'>
        <NavLink exact={true} activeClassName='active' to='/home'><i className="fas fa-home"></i></NavLink>
        <NavLink to='/employees' activeClassName='active'><i className="fas fa-users"></i></NavLink>
        <NavLink to='/projects' activeClassName='active'><i className="fas fa-product-hunt"></i></NavLink>
        <img src={`http://localhost:3333${localStorage.getItem('logo')}`} className='avatar' alt='ever thing is possible just try' />
      </div>
    );
  };
}
const mapState = store => ({
  company: store.login
})
export default connect(mapState)(AsideBar);