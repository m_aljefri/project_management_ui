import login from './reducers/loginReducer'
import employee from './reducers/employeeReducer'
import { combineReducers } from 'redux';
import {reducer as formReducer} from 'redux-form'

const rootReducer = combineReducers({
  login,
  employee,
  form: formReducer
});

export default rootReducer;