function setToken(token){
  if (token)
    localStorage.setItem('access-token', token)
}
export default function(state = {}, action){
  switch (action.type) {
    case 'LOGIN_PENDING':
      return {...state};
    case 'LOGIN_REJECTED':
      localStorage.removeItem('access-token');
      return {...state};
    case 'LOGIN_FULFILLED':
      setToken(action.payload.data.data.token.token);
      console.log(action.payload.data.data.token.company.logo.medium)
      localStorage.setItem('logo', action.payload.data.data.token.company.logo.medium)
      return {...state , company: action.payload.data.data.token.company}
    default:
      return {...state};
  }
}