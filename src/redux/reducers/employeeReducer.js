const INITIAL_STATE = {
  employees: [],
  employee: {},
  fetched: false,
  errMSg: '',
  succMSg: ''
}

let change = null

export default function(state = INITIAL_STATE, action){
  let employees = [];
  let employee = {};
  let pagination = {
    prev_page: null,
    next_page: null,
    current_page: null,
    total_pages: 0
  };
  switch (action.type){
    case 'CREAT_EMP':
    case 'EMP_LIST':
    case 'SHOW_EMP':
    case 'DESTROY_EMP':

    case 'CREAT_EMP_PENDING':
    case 'EMP_LIST_PENDING':
    case 'SHOW_EMP_PENDING':
    case 'DESTROY_EMP_PENDING':
      return {...state, errMSg: '', succMSg: '', fetched: false}
    case 'CREATE_EMP_REJECTED':
      change = {        
        errMSg: action.payload.response.data.errore,
        fetched: true
      }
      return {...state, ...change}
    case 'EMP_LIST_REJECTED':   
    case 'SHOW_EMP_REJECTED':
    case 'DESTROY_EMP_REJECTED':
      return {...state, errMSg: action.payload.response.data.errore, succMSg: ''}
    case 'CREATE_EMP_FULFILLED':
      change = {        
        succMSg: action.payload.data.message,
        fetched: true
      }
      return {...state, ...change}
    case 'EMP_LIST_FULFILLED': 
      change = {        
        employees: action.payload.data.data.employees,
        pagination: action.payload.data.data.pagination,
        fetched: true
      }

      return {...state, ...change }  

    case 'SHOW_EMP_FULFILLED':
      change = {        
        employee: action.payload.data.data,
        fetched: true
      }

      return {...state, ...change }

    case 'DESTROY_EMP_FULFILLED':
      
    default:
      return {...state, errMSg: '', succMSg: ''}
  }
}