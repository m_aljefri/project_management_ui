import axios from 'axios';


export const listEmployees = (page = 1) => {

  const url = `http://localhost:3333/en/v1/companies/employees?page=${page}`;
  let method= 'get';
  const result = axios({ method , url , headers: {Authorization: localStorage.getItem('access-token')} });
  return {
    type: 'EMP_LIST',
    payload: result
  }
}

export const createEmployee = data => {
  // console.log('create user dipatched');
  const url = `http://localhost:3333/en/v1/companies/employees`;
  let method= 'post';
  const result = axios({ method , url , data , headers: {Authorization: localStorage.getItem('access-token')} });
  return {
    type: 'CREATE_EMP',
    payload: result
  }
}

export const showEmployee = id => {
  
  const url = `http://localhost:3333/en/v1/companies/employees/${id}`;
  let method= 'get';
  const result = axios({ method , url , headers: {Authorization: localStorage.getItem('access-token')} });
  return {
    type: 'SHOW_EMP',
    payload: result
  }
}

export const editEmployee = (id, data) => {
  // console.log('create user dipatched');
  const url = `http://localhost:3333/en/v1/companies/employees/${id}`;
  let method= 'put';
  const result = axios({ method , url , data , headers: {Authorization: localStorage.getItem('access-token')} });
  return {
    type: 'UPDATE_EMP',
    payload: result
  }
}

export const destroyEmployee = (id) => {
  // console.log('create user dipatched');
  const url = `http://localhost:3333/en/v1/companies/employees/${id}`;
  let method= 'delete';
  const result = axios({ method , url , headers: {Authorization: localStorage.getItem('access-token')} });
  return {
    type: 'DESTROY_EMP',
    payload: result
  }
}
