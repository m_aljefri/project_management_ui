import axios from 'axios';


export const login = (data) => {

  const url = `http://localhost:3333/en/v1/company/session`;
  let method= 'post';
  const result = axios({ method , url , data});
  return {
    type: 'LOGIN',
    payload: result
  }
}