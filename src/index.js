import React from 'react';
import { render } from 'react-dom';
import './index.css';
import App from './components/App';
import Login from './components/login/login';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import {HashRouter, Switch, Route} from 'react-router-dom'
import RootRouter from './rootRouter'


import reducers from './redux/store';
import thunk from 'redux-thunk';
import promiseMiddleware from "redux-promise-middleware";
import REDUX_PROMISE_AXIOS from "redux-promise-axios";

import logger from 'redux-logger'
import { Redirect } from 'react-router';

 

const Store = createStore(reducers,
   applyMiddleware(thunk,logger,promiseMiddleware(), REDUX_PROMISE_AXIOS));

render(
  <Provider store={Store}>
    <HashRouter>
      <Switch>
        <Route path="/login" name="Login" component={Login}/>
        <Route path="/" name="Home" component={RootRouter}/>
        {/* <Redirect from='/' to='/home' /> */}
      </Switch>
    </HashRouter>
  </Provider>, document.getElementById('root'));
