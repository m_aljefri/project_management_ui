import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router';
import Dashboard from './dashboard/Dashbord';
import Employees from './employee/Employees';
import Header from '../layout/Header';
import AsideBar from '../layout/AsideBar';
import Footer from '../layout/Footer';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <AsideBar />
        <div className='view-space'>
          <Switch>
            <Route path='/home' name='dashbord' component={Dashboard} />
            <Route path='/employees' component={Employees} />
            <Redirect path='/' to='/home' />
          </Switch>
        </div>
        <Footer />
      </div>
    );
  }
}

export default App;
