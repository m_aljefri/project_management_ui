import React, { Component } from 'react'
import { connect } from 'react-redux'
//actions 
import {login} from '../../redux/actions/loginAction'
import { bindActionCreators } from 'redux';

class Login extends Component{
  constructor(props){
    super(props);

  }
  submit = (e) => {
    e.preventDefault();
    const user = {
      email_name: this.refs.email_name.value,
      password: this.refs.pwd.value
    }
    this.props.login(user)
  }

  redirectToApp = () => {
    const isLogged = localStorage.getItem('access-token') ? true : false ;
    const history = this.props.history;
    console.log(isLogged);
    if (isLogged) {
      history.replace('/');
    }
  }

  componentWillMount() {
    this.redirectToApp()
  }

  componentWillUpdate(){
    this.redirectToApp()
  }
  
  
  render(){
    return(
      <div className='col-sm-4 offset-sm-4 v-center'>
        <form onSubmit={this.submit}>
          <input 
            className='form-control mt-1'
            type='text'
            placeholder='Email or name'
            ref='email_name'
          />
          <input 
            className='form-control mt-1'
            type='password'
            placeholder='password'
            ref='pwd'
          />
          <button className='btn btn-primary mt-1'>login</button>
        </form>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({login}, dispatch)
};



export default connect(state => state, mapDispatchToProps)(Login);