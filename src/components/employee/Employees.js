import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import { Redirect } from 'react-router';
import ShowEmployee from './ShowEmployee';
import ListEmployees from './ListEmployees';
import CreateEmployee from './CreateEmployee';
import EditEmployee from './EditEmployee';

export default class Employees extends Component{
  render(){
    return(
      <Switch>
        <Route path='/employees/list' name='Employees list' component={ListEmployees} />
        <Route path='/employees/new' component={CreateEmployee} />
        <Route path='/employees/edit/:id' component={EditEmployee} />
        <Route path='/employees/:id' component={ShowEmployee} />
        <Redirect path="/employees" to='/employees/list' />
      </Switch>
    )
  }
}