import React, { Component } from 'react';
import {Field, reduxForm, getFormValues} from 'redux-form';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { editEmployee, showEmployee } from '../../redux/actions/employees'
import EmploteeForm from './EmployeeForm'

class EditEmployee extends Component{
  constructor(props){
    super(props);

    props.dispatch(showEmployee(props.match.params.id))
  }
  submit = values => {
    const { id } = this.props.match.params;
    this.props.dispatch(editEmployee(id, values))
  }
  render(){
    const {employee} = this.props
    return(
    <div className='mt-5 col-md-6 offset-md-3'>
      <EmploteeForm initialValues={{...employee}} onSubmit={this.submit}/>
    </div>
    )
    return (<h1>wait</h1>)
  }
}

const mapState = store => ({
  employee: store.employee.employee
})

export default EditEmployee = connect(mapState)(EditEmployee)



