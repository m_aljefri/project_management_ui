import React from 'react'
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { createEmployee } from '../../redux/actions/employees'
import { Link } from 'react-router-dom';
    
const validate = values => {
  const errors = {}
  if (!values.name) {
    errors.name = 'Required'
  } else if (values.name.length > 15) {
    errors.name = 'Must be 15 characters or less'
  }
  return errors
}

const warn = values => {
  const warnings = {};
  let birth = [];
  let join = [];
  const year = 0,month = 1, day = 2;
  if (values.birth_date && values.joining_date){
    birth = values.birth_date.split("-")
    join = values.joining_date.split("-")
  }
  if (birth[year] >= join[year]){
    console.log('bigger',birth[year] >= join[year]);
    warnings.birth_date = 'Hmm, you seem a bit young...'
  }else if (birth[month] >= join[month] || birth[day] >= join[day]){
    const age = join[year] - birth[year] - 1;
    if (age < 20) {
      console.log('age',age)
      warnings.birth_date = 'Hmm, you seem a bit young...'
    }
  }
  return warnings
}

const renderField = ({
  input,
  label,
  type,
  meta: { touched, error, warning }
}) => (
  <div>
    <label>{label}</label>
    <div>
      <input {...input} placeholder={label} type={type} 
      className='form-control mt-1' />
      {touched &&
        ((error && <span className='badge-danger'>{error}</span>) ||
          (warning && <span className='badge-warning'>{warning}</span>))}
    </div>
  </div>
)
const reset = values => {
  values.name = ''
  values.birth_date = ''
  values.joining_date = ''
}
let EmployeeForm = props =>{
  const { handleSubmit, pristine, reset, submitting } = props 
  return (
    <form onSubmit={handleSubmit}>
      <Field
        name='name'
        component={renderField}
        type='text'
        placeholder="Name" 
      />
      {(props.errMSg !== '')?<p className='badge-danger p-2'>{props.errMSg}</p>:
        (props.succMSg !== '')? <p className='badge-success p-2'>{props.succMSg}</p>:''
      }
      <label className='pb-0 m-0 mt-2'>Joining date</label>
      <Field
        className='form-control mt-1'
        name='joining_date'
        component={renderField}
        type='date'
        placeholder="Joinig date" 
      />
      <label className='pb-0 m-0 mt-2'>Birth date</label>
      <Field
        className='form-control mt-1'
        name='birth_date'
        component={renderField}
        type='date'
        placeholder="Birth date" 
      />
      <button type='submit' 
        className='btn btn-primary mt-1'>Create</button>
      <button onClick={reset} type='button' className='btn'>Reset</button>
        <Link to='/employees' className='btn'>Back to employees</Link>

    </form>
  )
}
const mapState = store => ({
 createEmployee: store.employee.createEmployee
 ,errMSg: store.employee.errMSg
 ,succMSg: store.employee.succMSg
})
EmployeeForm = connect(mapState)(EmployeeForm)

export default EmployeeForm = reduxForm({

  form: 'employee'
  ,validate // <--- validation function given to redux-form
  ,warn // <--- warning function given to redux-form
  ,enableReinitialize: true
})(EmployeeForm)



