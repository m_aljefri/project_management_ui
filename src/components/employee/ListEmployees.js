import React, { Component } from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {listEmployees, destroyEmployee} from '../../redux/actions/employees'
import {showEmployee} from '../../redux/actions/employees'
import { Link } from 'react-router-dom';

class ListEmployees extends Component{
  constructor(props){
    super(props);
    
    this.displayEmployees = this.displayEmployees.bind(this);
    this.displaypageNumber = this.displaypageNumber.bind(this);

    props.dispatch(listEmployees());
  }

  show = (id)=>{
    const {history} = this.props
    console.log('em id', id)
    history.replace(`/employees/show/:id${id}`);//redirect to show component
  } 

  reomve = (id)=>{
    this.props.dispatch(destroyEmployee(id));
    setTimeout(() => {
      this.props.dispatch(listEmployees());
    }, 1000);
  }

  // functio diplay possible pages for pagination
  displaypageNumber(){
    var i = 1;
    let pagesNumber = []
    console
    for(let i= 1; i <= this.props.pagination.total_pages; i++){
      (i == this.props.pagination.current_page)?
        pagesNumber.push(<li className="page-item active"><a className="page-link" >{i}</a></li>)
        :
        pagesNumber.push(<li className="page-item"><a className="page-link" onClick={()=>this.props.dispatch(listEmployees(i))}>{i}</a></li>);
    }
    return (pagesNumber)
  }

  //employe info card
  displayEmployees(employee){
    return (
    <div key={employee.id} className='f-l col-md-6 col-sm-12 mt-3'>
      <div className='emp col-sm-10 offset-sm-1'>
        <h4>{employee.name}</h4>
        <p>{employee.birth_date}</p>
        <p>{employee.joining_date}</p>
        <div className='actions'>
          <Link to={`/employees/${employee.id}`}>Show <i className='fa fa-eye'></i></Link>
          <Link to={`/employees/edit/${employee.id}`} className='edit'>Edit <i className='fa fa-edit'></i></Link>
          <button onClick={()=> this.reomve(employee.id)} className='remove'>Remove <i className='fa fa-user-times'></i></button>
        </div>
      </div>
    </div>
    )
  }

  openPage = page => {
    return this.props.dispatch(listEmployees(page));
  }
  
  render(){
    if (this.props.fetched){
    return(
  
      <div>
        <div className='create'>
          <Link to='/employees/new/' className='btn btn-primary'>Add new employee <i className='fa fa-user-plus'></i></Link>
        </div>
        <div className='row'>
        {console.log('employees',this.props.employees)}
         {this.props.employees.map( employ => {
           return this.displayEmployees(employ)
         })}
         </div>
        <div className='row center  mt-3 mb-3'>
          <ul className="pagination m-auto">
          {console.log('paginate',this.props.pagination)}
          {(this.props.pagination.prev_page === null)? '':
            <li className="page-item"><a onClick={() => this.openPage(this.props.pagination.prev_page)} className="page-link">Previous</a></li>
          } 
            {this.displaypageNumber()}
            {(this.props.pagination.next_page === null)? '':
            <li className="page-item"><a onClick={() => this.openPage(this.props.pagination.next_page)}className="page-link">Next</a></li>}
          </ul>
        </div>
      </div>
    );
    }
    return(
      <p>there is no employees</p>
    );
  }
}

const mapStateToProps = store => ({
  employees: store.employee.employees
  ,pagination: store.employee.pagination
  ,fetched: store.employee.fetched
})

export default connect(mapStateToProps)(ListEmployees);