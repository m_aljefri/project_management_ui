import React, { Component } from 'react';
import {Field} from 'redux-form';
import EmployeeForm from './EmployeeForm'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createEmployee } from '../../redux/actions/employees'

class CreateEmployee extends Component{
  submit = values => {
    console.log(values)
    this.props.dispatch(createEmployee(values))
  }
  render(){
    return(
    <div className='mt-5 col-md-6 offset-md-3'>
      <EmployeeForm onSubmit={this.submit}/>
    </div>
    )
  }
}

export default connect(null)(CreateEmployee)