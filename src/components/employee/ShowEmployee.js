import React, { Component } from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showEmployee } from '../../redux/actions/employees'
import { Link } from 'react-router-dom';

class ShowEmployee extends Component{
  constructor(props){
    super(props);

    props.dispatch(showEmployee(props.match.params.id))
  }
  displayRows = (task) => {
    return(
    <tr key={task.id}>
      <td>{task.id}</td>
      <td>{task.name}</td>
      <td>{task.description}</td>
      <td>{task.status}</td>
    </tr>
    )
  }
  
  render(){
    if(this.props.fetched){
      console.log(this.props)
    return(    
      <div>
        <Link to='/employees'>Back</Link>  
        <h1>employee info:</h1>
        <h3>Name: {this.props.employee.name}</h3>
        <h4>Project count: {this.props.employee.projects_count}</h4>
        <h1 className='mt-5'>tasks</h1>
        <div className="table-responsive">
          <table className="table table-dark table-striped">
            <thead>
              <tr>
                <th>ID </th>
                <th>NAME </th>
                <th>DESCRIPTION</th>
                <th>STATUS </th>
              </tr>
            </thead>
            <tbody>
              {(this.props.employee.tasks.map((e)=> this.displayRows(e)))}
            </tbody>
          </table>
        </div>
      </div>
    );}
    return(
      <h2>wait</h2>
    )
  }
}

const mapStateToProps = store => ({
  employee: store.employee.employee 
  ,fetched: store.employee.fetched 
})

export default connect(mapStateToProps)(ShowEmployee);